### Overview  
This software is a Java implementation of the the Cluedo board game.  
This program is a free software, you can redistribute and/or modify it. It was developed for the course of Object-Oriented Programming, faculty of Engineering and Computer Science, University of Bologna, Italy.  

### Usage instructions  
1. Download the jar file.  
2. Run it.  

### Libraries  
- [Guava](https://github.com/google/guava) - Google Core Libraries for Java  
- [JavaFX](http://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm)  

### Developers  
- Saccomanni Matteo (IT)  
- Renzoni Martina (IT)