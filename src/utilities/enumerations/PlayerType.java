package utilities.enumerations;

/**
 * This enumeration defines the types of players.
 */
public enum PlayerType {

    /**
     * The player is controlled by the PC.
     */
    AI,

    /**
     * The player is controlled by a person.
     */
    HUMAN;
}